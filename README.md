[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

# react-ld-json

A React component that renders ld+json script tags both on the browser and the server.

## Installation

`$ npm install react-ld-json`

## Usage

Import `react-ld-json` into your project.

```js
import LdJson from "react-ld-json";
```

Use the component by passing data for the structured data object.

```jsx
function MyComponent() {
  return (
    <LdJson
      data={
        {
          // ld+json object here.
        }
      }
    />
  );
}
```

# Contributing

Want to contribute? Great! Please branch out from the master version from using a branchname such as `feature/{insert-descriptive-feature-name}` and create a pull request when ready.

Use `npm run build` to build the project.
