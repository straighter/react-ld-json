import React from "react";

interface Props {
  data: { [key: string]: any };
}

export default function LdJson(props: Props) {
  return (
    <script
      type="application/ld+json"
      dangerouslySetInnerHTML={{ __html: JSON.stringify(props.data) }}
    />
  );
}
